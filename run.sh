#!/bin/bash

set -e
echo "Sleeping for 60 seconds to allow network interfaces to come up..."
sleep 60
python ~/dylospi/radio_dylos.py --start
