#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
######################################################################################################
#
# Dylos2PM2.5
#
# This module implements conversion and display of data from a Dylos DC1700 air quality monitor,
# as described in "Using a new, low-cost air quality sensor to quantify second-hand smoke (SHS)
# levels in homes", Sean Semple et al., Tobacco Control 2013
#
######################################################################################################

import sys
import os.path
import csv
import datetime
import math
import StringIO
import traceback
import appdirs

DEBUG = True
# Write debug log to user's local data directory
if not os.path.exists(appdirs.user_data_dir("DylosGUI")):
    os.makedirs(appdirs.user_data_dir("DylosGUI"))
DEBUG_LOG = os.path.join(appdirs.user_data_dir("DylosGUI"), "debug_log.txt")

def debug_log(*arg):
    if DEBUG is True:
        for i in arg:
            print i
        with open(DEBUG_LOG, 'a') as dl:
            for i in arg:
                dl.write(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:\t")
                         + str(i) + '\n')
debug_log("main Dylos2PM25 imports done")


def calculate_PM25_equiv(x):
    """
    Returns PM2.5 equivalent value calculated from small-large particles per
    cu. foot, per Semple et al. Tobacco Control Aug 2013 (2nd)
    Takes small - large particle value.
    """
    a = 1.57*(10**(-11))
    b = 4.16*(10**(-5))
    c = 0.65

    return a*(x**2) + b*x + c


def check_for_dylos_log(open_file):
    """
    Checks for headers and formatting of a dylos log file.
    Takes an open file object.
    """
    lines = open_file.readlines()
    if len(lines) < 8:
        return False
    elif "Particles per cubic foot" in lines[5]:
        return True
    elif "Particles per cubic foot" in lines[6]:
        return True
    else:
        return False


def get_log_type(file_object):
    """
    Was a log made by Dylos Logger or Python Dylos? This checks for key terms
    to see. It's not very advanced, but it does the job. Takes an open file
    object.
    """
    content = file_object.read()
    if "Python" in content:
        return "Python Dylos"
    elif "Logger" in content:
        return "Dylos Logger"
    else:
        return None


def read_file_to_dict(filename):
    """
    Returns a list of dictionaries of each line, without header values,
    from a log file.
    """
    open_file = open(filename, 'r')

    # Strip first 8 lines containing header values
    all_lines = open_file.readlines()[8:]

    all_dicts = []
    for i in all_lines:
        # Cover for blank lines under various systems
        if i == "\r\n" or i == "\n" or i == "\r":
            pass
        else:
            new_dict = {}
            list_of_values = i.replace(",", "").strip().split(" ")
            try:
                new_dict = {
                    "Date": list_of_values[0],
                    "Time": list_of_values[1],
                    "Small particles per cubic foot": float(list_of_values[2]),
                    "Large particles per cubic foot": float(list_of_values[3]),
                }
            except IndexError as e:
                debug_log("IndexError in new_dict (lines 64-69)")
                debug_log(e)
                debug_log(list_of_values)
                sys.exit()
            all_dicts.append(new_dict)
    return all_dicts


def read_string_to_dict(string_from_file):
    """
    Returns a list of dictionaries of each line, without header values, from
    a string of the contents of a log file. Useful if you want to copy and
    paste logs instead of opening a file.
    """
    open_log = StringIO.StringIO(string_from_file)

    # Strip first 8 lines containing header values
    all_lines = open_log.readlines()[8:]
    all_dicts = []
    for i in all_lines:
        # Covers for blank lines - might be worth checking for these anyway
        if i == "\n" or i == "\n" or i == "\r":
            pass
        else:
            new_dict = {}
            list_of_values = i.replace(",", "").strip().split(" ")
            if len(list_of_values) == 4:
                try:
                    new_dict = {
                        "Date": list_of_values[0],
                        "Time": list_of_values[1],
                        "Small particles per cubic foot":
                        float(list_of_values[2]),
                        "Large particles per cubic foot":
                        float(list_of_values[3]),
                    }
                except IndexError as error:
                    debug_log("IndexError")
                    debug_log(error)
                    debug_log(list_of_values)
                    sys.exit()
                all_dicts.append(new_dict)
            else:
                print "Ignoring short list"
    return all_dicts


def sort_dicts_by_date_time(dicts):
    """
    Sorts records from earliest to latest datetime. Useful if the dylos has
    returned records with a split. This can happen if the dylos has been set
    to the wrong time, records, then is set back to an earlier time and
    takes records without resetting the memory.

    Returns a list of dicts and a bool to say whether sorting changed the order:
        sorted_dicts, needed_sorted = sort_dicts_by_date_time(dicts)
    """
    sorted_dicts = sorted(dicts, key=datetime_from_record)
    if sorted_dicts == dicts:
        needed_sorted = False
    else:
        needed_sorted = True
    return sorted_dicts, needed_sorted


def calculate_values_dict(dicts):
    """
    Calculates small-large particles, PM2.5 equiv and percentage large particles
    Returns a list of dictionaries each containing date, time, small particles
    /cub.ft., lrg. part./cub.ft., sml.-lrg., PM2.5 equiv and %lrg part.
    Takes a list of dictionaries from read_file_to_dict()
    """
    complete_dicts = []
    for i in dicts:
        new_dict = i
        new_dict["Small-large particles per cu foot"] =\
            i["Small particles per cubic foot"] -\
            i["Large particles per cubic foot"]
        new_dict["PM2.5 equivalent (ug/m3)"] =\
            calculate_PM25_equiv(new_dict["Small-large particles per cu foot"])
        try:
            new_dict["% large particles"] = \
                (i["Large particles per cubic foot"] /
                (i["Small particles per cubic foot"]))*100
        except ZeroDivisionError:
            new_dict["% large particles"] = 0
        complete_dicts.append(new_dict)
    return complete_dicts


def str_date_from_datetime(dt):
    """
    Takes a datetime. Returns a date in DD/MM/YY as a string.
    """
    return dt.strftime("%d/%m/%y")


def str_time_from_datetime(dt):
    """
    Takes a datetime. Returns a time in HH:MM as a string.
    """
    return dt.strftime("%H:%M")


def scotify_dates(dicts):
    """
    Given a list of dictionaries with a "Date" key, this will correct the field
    always to use UK-standard dates (i.e. DD/MM/YYYY). This corrects a bug in
    the Dylos firmware, which records dates of day =<12 in UK format but days
    >12 in US format.
    Returns a list of dictionaries.
    """
    new_dicts = []
    for i in dicts:
        split_date = [int(x) for x in i["Date"].strip().split("/")]
        if split_date[0] < 13 and split_date[1] < 13:
            new_dicts.append(i)
        else:
            new_date = [split_date[1], split_date[0], split_date[2]]
            i["Date"] = str(new_date[0]) + "/" + \
                str(new_date[1]) + "/" + \
                str(new_date[2])
            new_dicts.append(i)
    return new_dicts


def scotify_all_dates(dicts):
    new_dicts = []
    for i in dicts:
        split_date = [int(x) for x in i["Date"].strip().split("/")]
        new_date = [split_date[1], split_date[0], split_date[2]]
        i["Date"] = str(new_date[0]) + "/" + \
            str(new_date[1]) + "/" + \
            str(new_date[2])
        new_dicts.append(i)
    return new_dicts


def strip_orphan_values(dicts, threshold=10):
    """
    A Dylos device records whenever it's on, leading to 'orphan' records at the
    start and end of logs, representing a reading taken after resetting the
    device but before placing it in a home, or after switching the device on
    but before downloading a log. This function takes a list of dictionaries in
    the usual format and returns one with any tops or tails shorter than the
    threshold (10 by default) in minutes.
    NOTE - This won't work if the dicts have been through
    generate_nil_records_for_time_breaks() as there will be records for every
    minute in that case.
    """
    new_dicts = []
    time_breaks = find_time_breaks(dicts)
    print time_breaks
    if time_breaks == []:
        return dicts
    elif (time_breaks[0]["start"] -
            datetime_from_record(dicts[0])).total_seconds() < (threshold*60):
        print "start"
        for i in dicts:
            if datetime_from_record(i) > time_breaks[0]["start"]:
                new_dicts.append(i)
    else:
        new_dicts = dicts
    new_dicts2 = []  # How ugly!

    if (datetime_from_record(new_dicts[-1]) -
            time_breaks[-1]["end"]).total_seconds() < (threshold*60):
        for i in new_dicts:
            if datetime_from_record(i) < time_breaks[-1]["end"]:
                new_dicts2.append(i)
        return new_dicts2
    else:
        return new_dicts


def datetime_from_record(record):
    """
    Given an individual record from a list of dicts, returns a datetime for that
    record. Note this assumes UK dates.
    """
    date = record["Date"]
    time = record["Time"]
    dt = date + " " + time
    dt_format = "%d/%m/%y %H:%M"
    return datetime.datetime.strptime(dt, dt_format)


def find_time_breaks(dicts):
    """
    Take dictionaries and find time discontinuities (i.e.
    periods when the Dylos has been switched off). Returns a list of
    dictionaries which include the datetime of the last record *before*
    switch-off (as key "start") and the first record *after* switch-on
    (as key "end").
    NOTE - this function expects dates in UK FORMAT ONLY. Run dictionaries
    through scotify_dates() first.
    """
    time_breaks = []
    #TODO: use datetimes for the below, this is crude
    try:
        start_time = [int(x) for x in dicts[0]["Time"].strip().split(":")]
    except IndexError as e:
        if dicts == []:
            debug_log("No data in dicts - passing")
            return None
        else:
            type_, value, tb = sys.exc_info()
            debug_log(traceback.print_exception(type_, value, tb))
            # print dicts
            sys.exit()
    start_date = [int(x) for x in dicts[0]["Date"].strip().split("/")]
    prev_datetime = datetime.datetime(year=start_date[2]+2000,
                                      month=start_date[1],
                                      day=start_date[0],
                                      hour=start_time[0],
                                      minute=start_time[1]) - \
        datetime.timedelta(seconds=60)
    for i in dicts:
        process_time = [int(x) for x in i["Time"].strip().split(":")]
        process_date = [int(x) for x in i["Date"].strip().split("/")]
        current_datetime = datetime.datetime(year=process_date[2]+2000,
                                             month=process_date[1],
                                             day=process_date[0],
                                             hour=process_time[0],
                                             minute=process_time[1])
        if current_datetime != prev_datetime + datetime.timedelta(seconds=60):
            time_breaks.append({
                "start": prev_datetime,
                "end": current_datetime,
            })
        prev_datetime = current_datetime
    return time_breaks


def get_data_times(dicts):
    """
    Gets time breaks, then converts them into records of time when air quality
    has been recorded. Takes the usual list of dictionaries.
    NOTE - this function expects dates in UK FORMAT ONLY. Run dictionaries
    through scotify_dates() first.
    """

    time_breaks = find_time_breaks(dicts)

    start = str(dicts[0]["Date"]) + " " + str(dicts[0]["Time"])
    start_datetime = datetime.datetime.strptime(start, "%d/%m/%y %H:%M")

    last = str(dicts[-1]["Date"]) + " " + str(dicts[-1]["Time"])
    last_datetime = datetime.datetime.strptime(last, "%d/%m/%y %H:%M")

    data_times = [{"start_data": start_datetime}]

    for i in time_breaks:
        data_times[-1]["end_data"] = i["start"]
        data_times.append({"start_data": i["end"]})

    data_times[-1]["end_data"] = last_datetime
    return data_times


def strip_between_datetimes(dicts, start, end):
    """
    Returns a list of dicts without those between the datetimes start and end.
    This is inclusive, so those at start and end will be removed too.
    Returns a list of dicts.
    """
    debug_log("Started stripping between datetimes: ")
    print start
    new_dicts = []
    for i in dicts:
        dt = datetime_from_record(i)
        # debug_log(dt)
        if dt >= start and dt <= end:
            # debug_log("Removed: ", i)
            pass
            # dicts.remove(i)
        else:
            new_dicts.append(i)
    return new_dicts


def generate_nil_records_for_time_breaks(dicts, time_breaks):
    """
    Takes dictionaries and a list of time_break dictionaries, as returned
    by find_time_breaks(). Returns a dictionary with additional records
    added for each minute when the device was inactive, with "0" (as a
    string) in every column other than "Date" and "Time".
    NOTE - this causes problems with averaging PM2.5.
    """

    for i in time_breaks:
        for j in dicts:
            if str_time_from_datetime(i["end"]) in j["Time"].strip() and \
                datetime.datetime.strptime(j["Date"], "%d/%m/%y").date() == \
                    i["end"].date():
                minutes_between = (i["end"] - i["start"])
                list_minutes = list(range(1, minutes_between.seconds/60))
                last_record = i["start"]
                keys = j.keys()
                keys.remove("Time")
                keys.remove("Date")
                new_minute_records = []
                for k in list_minutes:
                    new_record = {
                        "Time":
                        str_time_from_datetime(last_record +
                                               datetime.timedelta(seconds=60)),
                        "Date":
                        str_date_from_datetime(last_record +
                                               datetime.timedelta(seconds=60)),
                    }
                    last_record += datetime.timedelta(seconds=60)
                    for l in keys:
                        new_record[l] = "0"
                    new_minute_records.append(new_record)
                start_list = dicts[:dicts.index(j)]
                end_list = dicts[dicts.index(j):]
                new_list = start_list + new_minute_records + end_list
                dicts = new_list
    return dicts


def get_time_running(dicts):
    """
    Returns a dictionary containing the total number of minutes the device was
    running for, expressed as total minutes and days, hours and minutes.
    Takes a list of dictionaries as from read_file_to_dict().
    NOTE - this doesn't take account of time breaks, so it just expresses
    the total number of minutes the device was running in days, hours and
    minutes.
    """
    new_dict = {}
    new_dict["total minutes"] = len(dicts)
    a = datetime.timedelta(minutes=new_dict["total minutes"])
    new_dict["days"] = a.days
    hours_remainder_in_seconds = \
        (a - datetime.timedelta(days=a.days)).total_seconds()
    hours_float = hours_remainder_in_seconds/3600.00
    new_dict["hours"] = math.floor(hours_float)
    mins_remainder_in_seconds = \
        hours_remainder_in_seconds - float(new_dict["hours"]*3600)
    new_dict["minutes"] = math.floor(mins_remainder_in_seconds/60)
    return new_dict


def make_csv(dicts, filename="report.csv"):
    """
    Produces a comma-separated value file containing all time records and
    calculation results.
    NOTE - python dictionaries are unordered, so the headers are currently
    written any old way to the file. This will have to be input manually.
    """
    try:
        open_file = open(filename, "wb")
    except IOError:
        print "IOError: cannot open " + filename
        return False
    f_csv = csv.DictWriter(open_file, dicts[0].keys())
    f_csv.writeheader()
    for i in dicts:
        f_csv.writerow(i)
    return True


def get_maximum_PM25(dicts):
    """
    Returns the value of the largest PM2.5 estimate in a list of
    dictionaries as returned by calculate_values_dict().
    """
    return sorted(dicts,
                  key=lambda
                  k: float(k["PM2.5 equivalent (ug/m3)"]))[-1]["PM2.5 equivalent (ug/m3)"]


def get_mean_PM25(dicts):
    """
    Returns mean PM2.5 from a list of dicts which include a PM2.5 key.
    """
    all_PM25 = [float(x["PM2.5 equivalent (ug/m3)"]) for x in dicts]
    return sum(all_PM25)/float(len(all_PM25))


def get_time_over_25(dicts):
    """
    Returns the number of minutes when PM2.5 was more than 25ug/m3, the
    WHO's guideline limit for PM2.5 exposure. Takes a dictionary list
    which MUST already have PM2.5 estimates (per calculate_values_dict())
    """
    list_of_times = \
        [x for x in dicts if float(x["PM2.5 equivalent (ug/m3)"]) > 25.00]
    return len(list_of_times)


def get_time_over_arb(dicts, limit):
    """
    Returns the number of minutes when PM2.5 is above an arbitrary value
    in ug/m3. Takes a list of dicts (which have PM2.5 estimates per
    calculate_values_dict()) and a float limit.
    """
    list_of_times = \
        [x for x in dicts
            if float(x["PM2.5 equivalent (ug/m3)"]) > float(limit)]
    return len(list_of_times)


##def line_graph_large_particles(dicts,
##                               dpi=300,
##                               filename="large_particles.png"):
##    """
##    Plots percentage large particles by time using matplotlib
##    """
##
##    # Stylesheet for plots.
##    plt.style.use("afresh.mplstyle")
##    ax = plt.gca()
##
##    # Display grid and ticks below elements (why is this not the default!?)
##    ax.set_axisbelow(True)
##    xax = ax.get_xaxis()
##    ax.xaxis_date()
##    x_axis = [datetime.datetime.strptime(x["Time"]+"//" +
##                                         x["Date"], "%H:%M//%d/%m/%y")
##              for x in dicts]
##    y_axis = [x["% large particles"] for x in dicts]
##    plt.ylabel("% Large particles")
##    plt.xlabel("Time")
##    plt.title("% Large particles in the air over time")
##    plt.gcf().subplots_adjust(bottom=0.15)
##
##    first_time_record = datetime.datetime.strptime(dicts[0]["Time"] + "//" +
##                                                   dicts[0]["Date"],
##                                                   "%H:%M//%d/%m/%y")
##    debug_log("First time record: ", first_time_record)
##    last_time_record = datetime.datetime.strptime(dicts[-1]["Time"] + "//" +
##                                                  dicts[-1]["Date"],
##                                                  "%H:%M//%d/%m/%y")
##    debug_log("Last time record: ", last_time_record)
##    # If the difference between the start and end is more than a day and
##    # less than 2...
##    if (last_time_record - first_time_record).total_seconds() > 86400 and \
##            (last_time_record - first_time_record).total_seconds() < 2*86400:
##        xax.set_major_formatter(mdates.DateFormatter('%H:%M\n%a\n%d %b'))
##        loc = mdates.HourLocator([0, 6, 12, 18], interval=1)
##        xax.set_major_locator(loc)
##    # More than 2 days...
##    elif (last_time_record - first_time_record).total_seconds() >= 2*86400:
##        xax.set_major_formatter(mdates.DateFormatter('%H:%M\n%a\n%d %b'))
##        loc = mdates.HourLocator([12], interval=1)
##        xax.set_major_locator(loc)
##
##        # Add minor ticks
##        xax.set_minor_locator(mdates.HourLocator([0, 6, 12, 18], interval=1))
##    else:
##        xax.set_major_formatter(mdates.DateFormatter('%H:%M\n(%a)'))
##    yax = ax.get_yaxis()
##    yax.set_minor_locator(MultipleLocator(5))
##
##    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=1.0)
##
##    # Turn off x-axis grid, more closely resembles standard REFRESH style graphs
##    xax.grid(False)
##
##    plt.plot(x_axis, y_axis, 'g')
##    plt.savefig(filename, format="png", dpi=dpi)
##    plt.clf()
##
##    # # Fix for Firefox - won't load graphs otherwise
##    # filename = "file:///" + filename.replace("\\", "/")
##    # debug_log("Filename: " + filename)
##    return filename
##
##
##def line_graph_PM25(dicts, dpi=300, filename="figure.png"):
##    """
##    Plots PM2.5 concentration (y) by datetime (x) using matplotlib
##    """
##    # Stylesheet for plots.
##    plt.style.use("afresh.mplstyle")
##
##    x_axis = [datetime.datetime.strptime(x["Time"]+"//"+x["Date"],
##                                         "%H:%M//%d/%m/%y")
##              for x in dicts]
##
##    y_axis = [x["PM2.5 equivalent (ug/m3)"] for x in dicts]
##    plt.ylabel("PM2.5 (micrograms per cubic metre)")
##    plt.xlabel("Time")
##    plt.title("PM2.5 over time in your home")
##    # plt.text(x_axis[50], 650, "Red line shows WHO safe limit for PM2.5")
##    fig, g_ax = plt.subplots(1)
##    textstr = "Red line shows WHO safe limit for PM2.5"
##    props = dict(boxstyle='round', facecolor='brown', alpha=0.7)
##    g_ax.text(0.05,
##              0.95,
##              textstr,
##              transform=g_ax.transAxes,
##              fontsize=12,
##              verticalalignment='top',
##              bbox=props)
##
##    ax = plt.gca()  # Get current axes
##    # Display grid and ticks below elements (why is this not the default!?)
##    ax.set_axisbelow(True)
##    xax = ax.get_xaxis()
##    adf = xax.get_major_formatter()
##
##    plt.xlabel("Time")
##    plt.ylabel("PM2.5 concentration (ug/m3)")
##
##    first_time_record = datetime.datetime.strptime(dicts[0]["Time"] + "//" +
##                                                   dicts[0]["Date"],
##                                                   "%H:%M//%d/%m/%y")
##    last_time_record = datetime.datetime.strptime(dicts[-1]["Time"] + "//" +
##                                                  dicts[-1]["Date"],
##                                                  "%H:%M//%d/%m/%y")
##
##    # If the difference between the start and end is more than a day and
##    # less than two
##    if (last_time_record - first_time_record).total_seconds() > 86400 and \
##            (last_time_record - first_time_record).total_seconds() < 2*86400:
##        xax.set_major_formatter(mdates.DateFormatter('%H:%M\n%a\n%d %b'))
##        loc = mdates.HourLocator([0, 6, 12, 18], interval=1)
##        xax.set_major_locator(loc)
##
##    # Greater than/equal to 2 days...
##    elif (last_time_record - first_time_record).total_seconds() >= 2*86400:
##        xax.set_major_formatter(mdates.DateFormatter('%H:%M\n%a\n%d %b'))
##        loc = mdates.HourLocator([12], interval=1)
##        xax.set_major_locator(loc)
##        # Add minor ticks
##        xax.set_minor_locator(mdates.HourLocator([0, 6, 12, 18], interval=1))
##
##     # Greater than/equal to 5 days...
##    elif (last_time_record - first_time_record).total_seconds() >= 5*86400:
##        xax.set_major_formatter(mdates.DateFormatter('%H:%M\n%a\n%d %b'))
##
##        loc = mdates.HourLocator([12], interval=1)
##        xax.set_major_locator(loc)
##
##        # Add minor ticks
##        xax.set_minor_locator(mdates.HourLocator([0, 6, 12, 18], interval=1))
##
##    else:
##        xax.set_major_formatter(mdates.DateFormatter('%H:%M'))
##
##    plt.gcf().subplots_adjust(bottom=0.15)
##
##    yax = ax.get_yaxis()
##    yax.set_minor_locator(AutoMinorLocator())
##
##    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=1.0)
##
##    # Turn off x-axis grid, more closely resembles standard REFRESH style graphs
##    xax.grid(False)
##
##    plt.plot(x_axis,
##             y_axis,
##             "b-",
##             x_axis,
##             [25 for x in dicts],
##             "r--",
##             label="WHO guideline level")
##    plt.savefig(filename, format="png", dpi=dpi)
##    plt.clf()
##
##    # # Fix for Firefox - won't load graphs otherwise
##    # filename = "file://" + filename
##    return filename



